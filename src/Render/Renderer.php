<?php

/**
 * @file
 * Contains \Drupal\theme_system_sandbox\Render\Renderer.
 */

namespace Drupal\theme_system_sandbox\Render;

use Drupal\Core\Render\Renderer as DrupalRenderer;

/**
 * Overrides Drupal Core's renderer.
 */
class Renderer extends DrupalRenderer {}
